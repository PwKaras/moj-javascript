var products = window.products
var productsList = document.getElementById('productsList')
var cartItemsList = document.getElementById('cartItemsList')

productsList.innerText = ' '
cartItemsList.innerHTML = ' '
var productValueSumm = 0
for (var i = 0; i < products.length; i++) {
  var product = products[i]

  var datetxt = product.dataAdded.toLocaleDateString('pl')
  var grossPrice = (product.price * (1 + product.tax / 100)).toFixed(2)

  // "Ksiazka JavaScript" - PROMOCJA - 97.99PLN (20.01.2020)
  var text = product.name + ' - ' + (product.promo ? 'PROMOCJA - ' : '') + grossPrice + ' PLN ' + '(' + datetxt + ') '
  // utwórz klasę  css 'list-group-item'

  var listItemDiv = document.createElement('div')
  listItemDiv.classList.add('list-group-item')

  // Ustaw innerText = text

  listItemDiv.innerText = text
  // dodaj element do listy
  productsList.appendChild(listItemDiv)

  // dodawanie do koszyka
  var listCartNewDiv = document.createElement('div')
  listCartNewDiv.classList.add('list-group-item')
  listCartNewDiv.innerHTML = '<strong class="product-name">' + product.name + '</strong>' + ' ' + '<div class="product-price">' + grossPrice + '</div>'
  cartItemsList.appendChild(listCartNewDiv)
  productValueSumm += grossPrice

  console.log(text)
}

console.log('Suma produktów = ' + productValueSumm)
// stwórz element div z

// suma produktów

// Cart Intems list
var cartItems = window.cartItems
var totalPrice = 0

for (var i in cartItems) {
  var item = cartItems[i]
  var product = item.product
  var grossPrice = (product.price * (1 + product.tax / 100)).toFixed(2)
  totalPrice += grossPrice * item.amount
  console.log('Produkt' + product.name + ' - ' + grossPrice + ' x ' + item.amount + '=' + (item.amount * grossPrice))
}

document.querySelector('.cart-total-amount').innerHTML = totalPrice
